<?php

namespace App\Http\Controllers;

use App\Clientes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class ClientesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $dados =  Clientes::all();
        //print_r ($dados);
        //exit;
        return view('clientes_show', compact('dados'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        //return view('clientes_new');
        return 'passou pelo create';
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //$cliente = new Clientes;
        //$cliente->nome = $request->nome;
        //$cliente->profissao = $request->profissao;
        //$cliente->save();

        //echo "passou pelo store";

        //return redirect('/clientes');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\clientes  $clientes
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        //$dados = clientes::where('id', $id)->get();
        //return $dados;

        return Clientes::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\clientes  $clientes
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
        //
        //$cliente = Clientes::where('id',$id)->first();

        echo "passou pelo edit";

        //if(isset($cliente)) {
            //return view('editarcliente', compact('cliente'));
        //}
        //return redirect('/clientes/update/{id}');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\clientes  $clientes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        //
        //$cliente = Clientes::where('id',$id)->first();
        //if(isset($cliente)) {
            //$cliente->nome = $request->input('nomeCliente');
            //$cliente->save();
        //}
        //return redirect('/clientes');

        return 'atualizar';

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\clientes  $clientes
     * @return \Illuminate\Http\Response
     */
    public function destroy(Clientes $clientes)
    {
        //
        return 'destruir';
    }
}
