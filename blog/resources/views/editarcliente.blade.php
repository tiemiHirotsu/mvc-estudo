<div class="container">
    <form method="POST" action="/mvc-laravel/blog/public/clientes/update/{{$cliente->id}}">
        
        {{ csrf_field() }}
        
        <br>
        <label>Nome</label>
        <input type="text" name="nome" value="{{$cliente->nome}}">
        <br>
        <label>Profissão</label>
        <input type="text" name="profissao" value="{{$cliente->profissao}}">

        <input type="submit" value="Gravar">
    </form>
</div>